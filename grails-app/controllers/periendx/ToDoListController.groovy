package periendx

import grails.converters.JSON

/**
 * Controller to handle TODO List items
 * */
class ToDoListController {

    def toDoItemService

    def index() {
    }

    /**
     * This action invokes a service to fetch to do list
     * */
    def getToDoItems() {
        def data = toDoItemService.getTodoList()
        render data as JSON
    }

    /**
     * This action invokes a service to add new to do item in the list
     * */
    def addTodoItem() {
        def requestJson = request.JSON
        Map loadMap = [:]
        loadMap["dueDate"]= requestJson.dueDate
        loadMap["todoItem"] = requestJson.todoItem

        def data = toDoItemService.addTodoActionItem(loadMap)
        render data as JSON
    }

    /**
     * This action invokes a service to update to do item's status
     * */
    def updateStatus() {
        def id = params.id
        def data = toDoItemService.updateTodoStatus(id)
        render data
    }

    /**
     * This action invokes a service to edit to do item
     * */
    def editTodo() {
        Map updatedTodo = [:]
        updatedTodo["id"] = params.id
        updatedTodo["todoItem"] = params.todoItem
        updatedTodo["dueDate"] = params.dueDate
        def data = toDoItemService.editTodoItem(params)
        render data as JSON
    }

    /**
     * This action invokes a service to fetch to do item
     * */
    def fetchTodo() {
        def data = toDoItemService.fetchTodo(params.id)
        render data as JSON
    }

    /**
     * This action invokes a service to delete to do item
     * */
    def deleteTodo() {
        def data = toDoItemService.deleteTodo(params.id)
        render data
    }

    /**
     * This action invokes a service to sort to do list by due date
     * */
    def sortByDDate() {
        def data = toDoItemService.sortByDdate()
        render data as JSON
    }

    /**
     * This action invokes a service to sort to do list by ceration date
     * */
    def sortByCDate() {
        def data = toDoItemService.sortByCdate()
        render data as JSON
    }

    /**
     * This action invokes a service to search a to do item by text
     * */
    def searchByText() {
        def data = toDoItemService.searchByText(params.text)
        render data as JSON
    }

    /**
     * This action invokes a service to search a to do item by due date
     * */
    def searchByDueDate() {
        def data = toDoItemService.searchByDueDate(params.dueDate)
        render data as JSON
    }

    /**
     * This action invokes a service to search a to do item by due days
     * */
    def searchByDueDays() {
        def data = toDoItemService.searchByDueDays(Integer.parseInt(params.dueDays))
        render data as JSON
    }

    /**
     * This action invokes a service to fetch to do items by status
     * */
    def fetchTodoItemsByStatus() {
        def result = toDoItemService.fetchTodoItemsByStatus(params.status)
        render result as JSON
    }
}
//End: ToDoListController