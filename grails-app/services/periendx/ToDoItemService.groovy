package periendx

import grails.transaction.Transactional

@Transactional
class ToDoItemService {

    /**
     *This service adds new to do item in the to do list
     * @return newly added to do item
     * */
    def addTodoActionItem(def newTodo) {
        try {
            def today = new Date()
            def due = new Date().parse("yyyy-MM-dd", newTodo.dueDate)

            ToDoItem todoList = new ToDoItem(
                    "todoItem"      :       newTodo.todoItem,
                    "dueDate"       :       due,
                    "creationDate"  :       today,
                    "status"        :       "Pending"
            ).save(flush: true)
            return todoList
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service fetches whole to do to do list
     * @return to do list saved in db
     * */
    def getTodoList() {
        try {
            def todoList = ToDoItem.getAll()
            if(!todoList)
                return ["error": "Records not found"]
            Map returnMap = [:]
            todoList.each {
                List rowData = []
                rowData << it.id
                rowData << it.todoItem
                rowData << it.dueDate.format("yyyy-MM-dd")
                rowData << it.status
                rowData << it.creationDate.format("yyyy-MM-dd")
                returnMap["todo"+it.id]=rowData
            }
            return returnMap
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service updates to do item status
     * @return updated to do item
     * */
    def updateTodoStatus(def todoId) {
        try {
            ToDoItem updateTodo = ToDoItem.get(todoId)
            if(!updateTodo)
                return ["error": "Records not found"]
            updateTodo.status = "Done"
            updateTodo.save(flush: true)
            return updateTodo.status
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service edits to do item in the to do list
     * @return edited to do item
     * */
    def editTodoItem(Map updatedTodo) {
        try {
            def due = new Date().parse("yyyy-MM-dd", updatedTodo.dueDate)
            ToDoItem editTodo = ToDoItem.get(updatedTodo.id)
            if(!editTodo)
                return ["error": "Records not found"]
            editTodo.todoItem = updatedTodo.todoItem
            editTodo.dueDate = due
            editTodo.save(flush: true)
            return editTodo
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service is fetches single to do item from the to do list
     * @return to do item
     * */
    def fetchTodo(def id) {
        try {
            ToDoItem data = ToDoItem.get(id)
            if(!data)
                return ["error": "Records not found"]
            Map returnMap = ["todoItem":data.todoItem, "dueDate":data.dueDate.format("yyyy-MM-dd")]
            return returnMap
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service deleted to do item from the to do list
     * @return true if deleted
     * */
    def deleteTodo(def id) {
        try {
            ToDoItem deleteTodo = ToDoItem.get(id)
            if(!deleteTodo)
                return ["error": "Records not  found"]
            deleteTodo.delete()
            return true
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service sorts to do item by creation date
     * @return sorted to do list
     * */
    def sortByCdate() {
        try {
            def todoList = ToDoItem.list(sort: "creationDate")
            if(!todoList)
                return ["error": "Records not  found"]
            Map returnMap = [:]
            todoList.each {
                List rowData = []
                rowData << it.id
                rowData << it.todoItem
                rowData << it.dueDate.format("yyyy-MM-dd")
                rowData << it.status
                rowData << it.creationDate.format("yyyy-MM-dd")
                returnMap["todo"+it.id]=rowData
            }
            return returnMap
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service sorts to do items by due date
     * @return sorted to do list
     * */
    def sortByDdate() {
        try {
            def todoList = ToDoItem.list(sort: "dueDate")
            if(!todoList)
                return ["error": "Records not  found"]
            Map returnMap = [:]
            todoList.each {
                List rowData = []
                rowData << it.id
                rowData << it.todoItem
                rowData << it.dueDate.format("yyyy-MM-dd")
                rowData << it.status
                rowData << it.creationDate.format("yyyy-MM-dd")
                returnMap["todo"+it.id]=rowData
            }
            return returnMap
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service search to do item by due date
     * @return to do list of matching criterion
     * */
    def searchByDueDate(def dueDate) {
        try{
            Date due = new Date().parse("yyyy-MM-dd", dueDate) //.format("yyyy-MM-dd")
            def todoDetailsByDDate = ToDoItem.findAllByDueDate(due, [readOnly: true])
            if(!todoDetailsByDDate)
                return ["error": "Records not  found"]
            Map returnMap = [:]
            todoDetailsByDDate.each {
                List rowData = []
                rowData << it.id
                rowData << it.todoItem
                rowData << it.dueDate.format("yyyy-MM-dd")
                rowData << it.status
                rowData << it.creationDate.format("yyyy-MM-dd")
                returnMap["todo"+it.id]=rowData
            }
            return returnMap
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service search to do item by due days
     * @return to do list of matching criterion
     * */
    def searchByDueDays(Integer days) {
        try{
            Date today = new Date()
            def todoDetailsByDDate = ToDoItem.findAllByDueDateGreaterThanAndDueDateLessThan(today, today+days, [readOnly: true])
            if(!todoDetailsByDDate)
                return ["error": "Records not  found"]
            Map returnMap = [:]
            todoDetailsByDDate.each {
                List rowData = []
                rowData << it.id
                rowData << it.todoItem
                rowData << it.dueDate.format("yyyy-MM-dd")
                rowData << it.status
                rowData << it.creationDate.format("yyyy-MM-dd")
                returnMap["todo"+it.id]=rowData
            }
            return returnMap
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service search to do item by text
     * @return to do list of matching criterion
     * */
    def searchByText(def text) {
        try {
            def todoDetailsByText = ToDoItem.findAllByTodoItemLike("%"+text+"%", [readOnly: true])
            if(!todoDetailsByText)
                return ["error": "Records not  found"]
            Map returnMap = [:]
            todoDetailsByText.each {
                List rowData = []
                rowData << it.id
                rowData << it.todoItem
                rowData << it.dueDate.format("yyyy-MM-dd")
                rowData << it.status
                rowData << it.creationDate.format("yyyy-MM-dd")
                returnMap["todo"+it.id]=rowData
            }
            return returnMap
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }

    /**
     *This service fetched to do items by status
     * @return to do list of matching status
     * */
    def fetchTodoItemsByStatus(def status) {
        try {
            def todoDetailsByText = ToDoItem.findAllByStatus(status, [readOnly: true])
            if(!todoDetailsByText)
                return ["error": "Records not found"]
            Map returnMap = [:]
            todoDetailsByText.each {
                List rowData = []
                rowData << it.id
                rowData << it.todoItem
                rowData << it.dueDate.format("yyyy-MM-dd")
                rowData << it.status
                rowData << it.creationDate.format("yyyy-MM-dd")
                returnMap["todo"+it.id]=rowData
            }
            return returnMap
        } catch (Exception e) {
            print e.printStackTrace()
        }
    }
}
//End: ToDoItemService