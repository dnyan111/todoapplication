<%--
  Created by IntelliJ IDEA.
  User: Dnyan
  Date: 3/9/2017
  Time: 10:49 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>To Do List Dashboard</title>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/twitter-bootstrap/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/jquery-ui-1.11.2/jquery-ui.css" type="text/css" />
    <!-- Java Script Libraries -->
    <script src="<%=request.getContextPath() %>/css/jquery-ui-1.11.2/external/jquery/jquery.js"></script>
    <script src="<%=request.getContextPath() %>/css/jquery-ui-1.11.2/jquery-ui.js"></script>
    <script src="<%= request.getContextPath() %>/css/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="<%= request.getContextPath() %>/css/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>

</head>

<body>
<div>
    <label>Todo Item: </label><input type="text" placeholder="To do Item"/>
</div>
<div>
    <label>Due Date: </label><input type="datetime" placeholder="Due Date"/>
</div>
<div>
    <button>Add</button>
</div>

<div>
    <button onclick="getTodoItems()">Show all Items</button>
</div>
<div id="to-do-list">

</div>
</body>

<script>
    $(function(){

    });

    function getTodoItems() {
        $.getJSON('<g:createLink controller="ToDoList" action="getToDoItems"/>',function(data){
            $("#to-do-list").append(data);
        });
    }

</script>
</html>